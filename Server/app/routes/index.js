const router = require('express').Router()


/** 
 * middlewares
 */
// ------------------------------------ //

const token = require('../middlewares/token')
const access = require('../middlewares/access')

// ------------------------------------ //


/** 
 * routes
*/
// ------------------------------------ //
const public = require('./public')
const admin = require('./admin')
const profile = require('./profile')

// ------------------------------------ //


router.use('/', public)
router.use('/admin', token.check(true), access.check(['admin']), admin)
router.use('/profile', token.check(true), profile)


/**
 * error handler
 */
// ---------------- Start -------------------- //
router.use((req, res) => {
    return res.status(404).json({
        status: 404,
        devMSG: 'not found',
        userMSG: 'این درخواست وجود ندارد'
    })
})

router.use((err, req, res, next) => {
    if (err.name === "MulterError") {
        return res.status(415).json({
            status: 400,
            devMSG: err.message,
            userMSG: "حجم فایل باید کم تر از 50 مگابایت باشد"
        })
    }
    else {
        return res.status(err.status || 500).json({
            status: err.status || 500,
            devMSG: err.message,
            userMSG: err.userMSG,
            errors: err.errors
        })
    }
})
// ----------------- END --------------------- //


module.exports = router