const router = require('express').Router()

/**
 * controllers
 */
// ------------------------------------ //

const auth = require('../controllers/auth/auth')
const refreshToken = require('../controllers/auth/refresh_token.auth')
const getEvents = require('../controllers/public/Get_Events.controller')


// ------------------------------------ //

/**
  middlewares
 */
// ------------------------------------ //

const access = require('../middlewares/access')
const token = require('../middlewares/token')

// ------------------------------------ //


router.post('/auth', auth.send_sms)
router.post('/auth/verify', auth.checkVerifyCode)
router.post('/auth/singup', auth.singUpUser)
router.get('/auth/access_token', token.check(false), refreshToken)
router.get('/events', getEvents.handle)

module.exports = router