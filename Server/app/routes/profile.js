const router = require('express').Router()

/**
 * controllers
 */
// ------------------------------------ //
const getUserProfile = require('../controllers/profile/get_user_info.profile')
const editUserProfile = require('../controllers/profile/edit_user_info.profile')
// ------------------------------------ //

/**
  middlewares
 */
// ------------------------------------ //

// ------------------------------------ //


router.get('/', getUserProfile.handle)
router.put('/', editUserProfile.handle)



module.exports = router