const validator = require('validator')
const chalk = require('chalk');
const log = console.log;

exports.validate = async (phoneNumber, next) => {
    try {
        const errors = [];

        if (validator.default.isEmpty(phoneNumber))
            errors.push({ type: 'phoneNumber', err: 'شماره موبایل الزامیست' })

        else if (!validator.default.isMobilePhone(phoneNumber, ['fa-IR']))
            errors.push({ type: 'phoneNumber', err: 'شماره موبایل وارد شده معتبر نیست' })


        if (!errors.length) {
            return {
                status: 'ok'
            }
        }

        const error = new Error()
        error.status = 400
        error.devMSG = ''
        error.userMSG = 'لطفا مقادیر وارد شده را مجدد بررسی کنید'
        error.errors = errors
        return {
            status: 'notOk',
            error
        }

    } catch (err) {
        log(chalk.whiteBright.bgRed.bold('validtor => sendSMS.validator.js'))
        log(err)
        let error = new Error(`Internal Server Error`)
        error.status = 500
        error.userMSG = 'لطفا بعدا امتحان کنید'
        next(error)
        throw error
    }
}