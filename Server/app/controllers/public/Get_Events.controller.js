const Event = require('../../models/Event')
const chalk = require('chalk');
const log = console.log;

exports.handle = async (req, res, next) => {
    try {
        const {
            count
        } = req.query

        let result = [];
        if (count)
            result = await Event.find({}).limit(count)
        else
            result = await Event.find({
                date: {
                    $gte: Date.now()
                },
            }).limit(4)


        return res.json(result)
    } catch (err) {
        log(chalk.whiteBright.bgRed.bold('controller => user = Add_EvenGet_Events.controllert.js'))
        log(err)
        let error = new Error(`Internal Server Error`)
        error.status = 500
        error.userMSG = 'لطفا بعدا امتحان کنید'
        res.status(500).json(error)
        throw error
    }
}