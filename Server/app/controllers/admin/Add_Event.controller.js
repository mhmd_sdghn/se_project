const Event = require('../../models/Event')
const chalk = require('chalk');
const log = console.log;

exports.handle = async (req, res, next) => {
    try {
        const {
            title,
            content,
            date
        } = req.body
        let image = null
        if (req.file)
            image = `${req.file.destination.substring(9)}/${req.file.filename}`

        await Event.create({
            title,
            creator: req.user.id,
            content,
            date,
            image
        })
        return res.status(201).send()
    } catch (err) {
        log(chalk.whiteBright.bgRed.bold('controller => admin = Add_Event.controller.js'))
        log(err)
        let error = new Error(`Internal Server Error`)
        error.status = 500
        error.userMSG = 'لطفا بعدا امتحان کنید'
        res.status(500).json(error)
        throw error
    }
}