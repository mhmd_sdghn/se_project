const Event = require('../../models/Event')
const chalk = require('chalk');
const log = console.log;

exports.handle = async (req, res, next) => {
    try {
        const { id } = req.query

        await Event.findByIdAndRemove(id)

        res.send()
    } catch (err) {
        log(chalk.whiteBright.bgRed.bold('controller => admin = Delete_Event.controller.js'))
        log(err)
        let error = new Error(`Internal Server Error`)
        error.status = 500
        error.userMSG = 'لطفا بعدا امتحان کنید'
        res.status(500).json(error)
        throw error
    }
}