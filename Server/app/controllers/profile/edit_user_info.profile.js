const chalk = require('chalk');
const dataCheck = require('../../validators/edit_profile_info.validator')
const log = console.log;

exports.handle = async (req, res, next) => {
    try {

        const {
            name,
            email
        } = req.body

        let user = req.user

        let validate_result = await dataCheck.validate(name, email, next)
        if (validate_result.status == 'notOk')
            return next(validate_result.error)
            
        await user.update({
            name: name,
            email: email
        })

        return res.json({
            status: 200,
            devMSG: '',
            userMSG: 'اطلاعات بروز شد'
        })
    } catch (err) {
        log(chalk.whiteBright.bgRed.bold('controller => profile = edit_user_info.profile.js'))
        log(err)
        let error = new Error(`Internal Server Error`)
        error.status = 500
        error.userMSG = 'لطفا بعدا امتحان کنید'
        res.status(500).json(error)
        throw error
    }
}