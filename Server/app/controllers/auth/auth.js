const autoBind = require('auto-bind');
const User = require('../../models/User')
const Register = require('../../models/Register')
const sms = require('../../helpers/sms')
const chalk = require('chalk');
const log = console.log;
const singupValidate = require('../../validators/singup.validate')
const sendSmsValidator = require('../../validators/sendSMS.validator')

class auth {
    constructor() {
        autoBind(this);
    }


    /**
     * search for phone number
     */

    async checkPhoneNumber(phoneNumber) {
        let error = null

        let user;
        user = await Register.findOne({
            phoneNumber
        })

        if (user) return {
            type: 'incompleteRegister',
            user
        }

        user = await User.findOne({
            phoneNumber
        })

        if (user) return {
            type: 'user',
            user
        }

        return {
            type: 'register'
        }
    }

    /**
     * check verify code
     */

    async checkVerifyCode(req, res, next) {
        const { code, phoneNumber } = req.body

        const error = new Error()

        if (!phoneNumber) {
            error.message = ''
            error.status = 400
            error.devMSG = "phoneNumber required"
            next(error)
            return
        }

        const data = await this.checkPhoneNumber(phoneNumber)




        if (data.type == 'register') {

            error.status = 400
            error.message = ''
            error.userMSG = 'ابتدا برای ارسال کد فعال سازی اقدام کنید'
            next(error)
            return
        }
        else if (data.user.verifyCode == code) // check for verifu code
            if (data.user.verifyCodeExpire > Date.now()) // check for verify 
            {
                if (data.type == 'user') {
                    const result = await data.user.generateToken()
                    if (result.status == 'notOk')
                        return next(result.error)

                    else
                        return res.json({
                            status: 200,
                            devMSG: '',
                            type: data.user.role,
                            ...result
                        })
                }
                else if (data.type == 'incompleteRegister') {
                    data.user.verifyStatus = true
                    await data.user.save()
                    return res.json({
                        status: 200,
                        devMSG: 'send req to "/auth/singup" route with this number'
                    })
                }
            }
            else {
                error.status = 400
                error.message = ''
                error.userMSG = 'یکبار مصرف منقضی شده'
                next(error)
                return
            }
        else {
            error.status = 400
            error.message = ''
            error.userMSG = 'کد وارد شده نا درست است'
            next(error)
            return
        }


    }

    /**
     * send verify code
     */

    async send_sms(req, res, next) {
        try {

            const { phoneNumber } = req.body


            let validate_result = await sendSmsValidator.validate(phoneNumber, next)
            if (validate_result.status == 'notOk')
                return next(validate_result.error)

            /**
             * check phone number in db
             */

            const data = await this.checkPhoneNumber(phoneNumber)

            /**
             * generate random number for verify user
             */

            const randomNumber = Math.floor(Math.random() * (99999 - 10000 + 1)) + 10000

            /**
             * if user was completly new in system
             */

            if (data.type == 'register') {
                await Register.create({
                    phoneNumber,
                    verifyCode: randomNumber,
                    verifyCodeExpire: Date.now() + 1000 * 60
                })

            }

            else {
                data.user.verifyCode = randomNumber // data contain user info
                data.user.verifyCodeExpire = Date.now() + 1000 * 60
                await data.user.save() // save randomNumber in db
            }

            /**
             * send verify code to user
             */

            sms.SendMessageForVerify(phoneNumber, randomNumber, 14044)
            return res.json({
                status: 200,
                devMSG: '',
                userMSG: 'کد فعال سازی برای شما ارسال شد'
            })

        } catch (err) {
            log(chalk.whiteBright.bgRed.bold('controller => auth = auth.js'))
            log(err)
            let error = new Error(`Internal Server Error`)
            error.status = 500
            error.userMSG = 'لطفا بعدا امتحان کنید'
            res.status(500).json(error)
            throw error
        }
    }


    /**
     * save user data in db 
     */

    async singUpUser(req, res, next) {
        try {
            const {
                phoneNumber,
                email,
                password,
                name
            } = req.body

            const validate = await singupValidate.validate(email, password, name, next)

            if (validate.status == 'notOk')
                return next(validate.error)

            const register = await Register.findOne({
                phoneNumber,
                verifyStatus: true
            })

            if (!register) {
                const error = new Error()
                error.status = 400
                error.devMSG = ''
                error.userMSG = 'شماره موبایل شما تایید نشده'
                return next(error)
            }

            await register.remove()

            let user = await User.create({
                phoneNumber,
                email,
                name
            })

            const hash = await user.hashPassword(password)
            user.password = hash
            await user.save()

            const objectToken = await user.generateToken()
            if (objectToken.status == 'notOk')
                return next(objectToken.error)

            return res.json({
                status: 200,
                type: user.role,
                access: objectToken
            })

        } catch (err) {
            log(chalk.whiteBright.bgRed.bold('controller => auth => auth.js => singUpUser'))
            log(err)
            let error = new Error(`Internal Server Error`)
            error.status = 500
            error.userMSG = 'لطفا بعدا امتحان کنید'
            res.status(500).json(error)
            throw error
        }
    }


}

module.exports = new auth