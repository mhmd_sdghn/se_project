const mongoose = require('mongoose')


const WH = new mongoose.Schema({
    submitedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    employee: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    date: String,
    time: String

}, { timestamps: true, toJSON: { virtuals: true } })

WH.index({ title: 'text', content: 'text' })

module.exports = mongoose.model('wh', WH)