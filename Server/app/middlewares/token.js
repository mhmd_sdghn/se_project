const User = require('../models/User')
const chalk = require('chalk');
const log = console.log;
const jwt = require('jsonwebtoken')


exports.check = function (ignoreExpire) {
    return async function (req, res, next) {
        try {

            let authorization = req.get('Authorization')
            let error = null

            /**
             * if req token doesn't exist
             */

            if (!authorization) {
                error = new Error()
                error.message = 'need authorization token'
                error.status = 401
                error.userMSG = ''
                next(error)
                return
            }

            /**
             * remove 'Bearer ' from  authorization strign
             */

            const token = authorization.replace('Bearer ', '');

            /**
             * verify token
             */

            jwt.verify(token, process.env.JSON_WEB_TOKEN_PRIVATE_KEY,
                { ignoreExpiration: ignoreExpire || false },
                async (err, decoded) => {
                    if (err) {
                        error = new Error()
                        error.message = 'token has been expired'
                        error.status = 401
                        error.userMSG = ''
                    }
                    else {
                        const user = await User.findOne({
                            _id: decoded.id
                        })

                        /**
                         * if decoded user id doesn't exist
                         */
                        if (!user) {
                            error = new Error()
                            error.message = 'invalid token'
                            error.status = 401
                            error.userMSG = ''
                            next(error)
                        }
                        else {
                            req.user = user
                            return next()
                        }
                    }
                })

            /**
            * if auth failed and error exist server send error as response
            */



            if (error) return next(error)


        } catch (err) {
            log(chalk.whiteBright.bgRed.bold('middleware => token.js'))
            log(err)
            let error = new Error(`Internal Server Error`)
            error.status = 500
            error.userMSG = 'لطفا بعدا امتحان کنید'
            res.status(500).json(error)
            throw error
        }
    }
}